"""The main program"""

import json
import sys

import corelib
import tree


def get_args_of_input(userinput):
    """Strip the first item from a list of tokenized user input"""

    args = userinput
    args.pop(0)

    return args


def do_action(worldnode, userinput):
    """Get list of scripts, unconditionally run user's command"""

    playernode = tree.getnode_named_in('player', worldnode)
    worldactions = corelib.ACTIONS
    verb = userinput[0]
    args = get_args_of_input(userinput)

    worldactions[verb](worldnode, playernode, args)


def get_agents_actions(name, world):
    """Get collection of actions that <name> is capable of"""

    agentnode = tree.getnode_named_in(name, world)
    agentbody = tree.getbody_of(agentnode)
    actions = agentbody['actions']

    return actions


def attempt_action(world, userinput):
    """Attempt to execute an actors command"""

    verb = userinput[0]
    playeractions = get_agents_actions('player', world)
    worldactions = corelib.ACTIONS

    if not verb in worldactions:
        print("Nobody knows how to do that.")
        return
    if not verb in playeractions:
        print("You do not know how to do that.")
        return

    do_action(world, userinput)


def main():
    """The main entry point, where the program starts running"""

    if len(sys.argv) != 2:
        print('usage:', sys.argv[0], '<gamefile>')
        sys.exit()

    print('Opening gamefile:', sys.argv[1])
    try:
        file = open(sys.argv[1])
        world = json.load(file)
    except:  # pylint: disable=bare-except
        print('Shit, that gamefile be wack yo!')
        print('Exiting.')
        sys.exit()

    print('Welcome to TextBros!')
    print('Type some commands.')
    while True:
        i = input().split()
        attempt_action(world, i)


if __name__ == "__main__":
    main()
