# Experiment: "Everything is a dictionary".

import json


# Create some rooms and stuff

world = {}
room = {}
cupboard = {}
hallway = {}

world['room'] = room
world['hallway'] = hallway
room['cupboard'] = cupboard

room['smell'] = 'pretty foul'
cupboard['content'] = 'nothing'
hallway['length'] = 'about average'


# Relate rooms

room['north'] = 'hallway'
hallway['south'] = 'room'


# Have a look

print(json.dumps(world, indent=4))
