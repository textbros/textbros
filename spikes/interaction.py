# How can a player do an action relating to another object?
#
# (Greatly simplifying internals, for the sake of spiking).
#
# Incidentally, this spike turned into a test of finding parents
# in nested dictionaries.
# (It would be easier to use and understand if they were split in two spikes).
#
# Woopsa... It turned out quite messy.


# Here are some handy functions for traversing nested dictionaries:

def find_path(prevname, d, needle):
    for k, v in d.items():
        if k == needle:
            return [prevname]
        elif isinstance(v, dict):
            p = find_path(k, v, needle)
            if p:
                return [prevname] + p


def find_body(d, path):
    path.pop(0)  # Go one step down the path
    for k, v in d.items():
        if k == path[0]:
            return v
    # This function is actually incomplete


# Define a world to play in
world = {}
room = {}
player = {}
player['actions'] = {}


def look():
    parentpath = find_path('world', world, 'player')
    parentdict = find_body(world, parentpath)
    reaction = parentdict['reactions']['look']  # TODO check first
    print(reaction)


player['actions']['look'] = look
room['player'] = player
room['reactions'] = {}
room['reactions']['look'] = 'It is just a room'
world['room'] = room


# Test if everything works as expected

# What does the world of nested dictionaries look like?
print('The world looks like {}'.format(world))

# Does the find_path function work?
for item in ['actions', 'look', 'player', 'room', 'world', 'poo']:
    parents = find_path('world', world, item)
    print('{} found in {}'.format(item, parents))

# Test if actions can interact between objects
while True:
    i = input('tb: ')
    if i in player['actions']:
        player['actions'][i]()
