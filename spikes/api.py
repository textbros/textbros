# Contemplating engine's api

import json


def createnode(name):
    return {name: {}}


def appendnode(donee, new):
    doneebody = getnodebody(donee)
    newname = getnodename(new)
    newbody = getnodebody(new)

    doneebody[newname] = newbody


def getnodename(node):
    keys = node.keys()
    keylist = list(keys)

    return keylist[0]


def getnodebody(node):
    name = getnodename(node)
    return node[name]


world = createnode("world")

room = createnode("room")
appendnode(world, room)
cupboard = createnode("cupboard")
appendnode(room, cupboard)

appendnode(room, createnode("player"))
appendnode(world, createnode("hallway"))

print(world)
print(json.dumps(world, indent=4))
